package com.example.simone.rssfeed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.example.simone.rssfeed.RSSItem;

public class RssList extends Activity  {

    String feedUrl = "";
    ListView rssListView = null;
    ArrayList<RSSItem> RSSItems = new ArrayList<RSSItem>();
    ArrayAdapter<RSSItem> array_adapter = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_list);

        feedUrl = "http://feeds.feedburner.com/Blogrammazione/";

        refreshRSSList();

        rssListView = (ListView) findViewById(R.id.rssListView);

        array_adapter = new ArrayAdapter<RSSItem>(this, R.layout.activity_rss_item, RSSItems);
        rssListView.setAdapter(array_adapter);

        refreshRSSList();

        rssListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {// not sure it's correct
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        RSSItem item = (RSSItem) parent.getItemAtPosition(position);//here I extract the object from the ListView

                        String itemUrl = String.valueOf(item.getLink());//here I extract the link from the object
                        Intent i = new Intent(RssList.this, WebPage.class);
                        i.putExtra("url", itemUrl);// data exchange between two activites (url)

                        RssList.this.startActivity(i);

                    }

                }

        );
    }

    private void refreshRSSList() { //here I reload feed data in order to refresh the feedList

        ArrayList<RSSItem> newItems = RSSItem.getRSSItems(feedUrl);
        RSSItems.clear();
        RSSItems.addAll(newItems);

    }

}