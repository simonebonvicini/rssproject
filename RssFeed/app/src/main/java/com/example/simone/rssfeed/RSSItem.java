package com.example.simone.rssfeed;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class RSSItem {

    private String title;
    private String description;
    private Date date;
    private String link;

    public RSSItem(String title, String description, Date pubDate, String link) {
        this.title= title;
        this.description= description;
        this.date= pubDate;
        this.link= link;
    }

    //Getters&setters stuff

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPubDate() {
        return date;
    }

    public void setPubDate(Date pubDate) {
        this.date = pubDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {  //format stuff: title+data

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm - MM/dd/yy");

        String result = getTitle() + "   ( " + sdf.format(this.getPubDate()) + " )" + getLink();
        return result;
    }

    public static ArrayList<RSSItem> getRSSItems(String feedUrl) { //the nucleus of the app: it checks the connection, parses the feed, and stores itemData

        ArrayList<RSSItem> rssItems = new ArrayList<RSSItem>();

        try {

            URL url = new URL(feedUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) { //if connection is fine
                InputStream is = conn.getInputStream();

                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();//I start preparing parsing stuff
                DocumentBuilder db = dbf.newDocumentBuilder();


                Document document = db.parse(is); //then I save the parse result into Document document
                Element element = document.getDocumentElement(); //Document is made of elements


                NodeList nodeList = element.getElementsByTagName("item"); //every item is a Node, so I create a NodeList to store them all

                if (nodeList.getLength() > 0) {   //for cycle to get every info about every item
                    for (int i = 0; i < nodeList.getLength(); i++) {

                        Element entry = (Element) nodeList.item(i);
                        Element _titleE = (Element)entry.getElementsByTagName("title").item(0);
                        Element _descriptionE = (Element)entry.getElementsByTagName("description").item(0);
                        Element _pubDateE = (Element) entry.getElementsByTagName("pubDate").item(0);
                        Element _linkE = (Element) entry.getElementsByTagName("link").item(0);
                        String _title = _titleE.getFirstChild().getNodeValue();
                        String _description = _descriptionE.getFirstChild().getNodeValue();
                        Date _pubDate = new Date(_pubDateE.getFirstChild().getNodeValue());
                        String _link = _linkE.getFirstChild().getNodeValue();

                        RSSItem rssItem = new RSSItem(_title, _description, _pubDate, _link);

                        rssItems.add(rssItem); //Here I create a new RSSitem object and I add it to the RSSItems list

                    }
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return rssItems;
    }
}