package com.example.simone.rssfeed;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;


public class WebPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_page);

        Bundle listItemUrl = getIntent().getExtras(); //here I get data from the i.putExtras
        if(listItemUrl==null){
            return ;
        }
        String itemUrl = listItemUrl.getString("url");


        Log.i("url",itemUrl );

        final WebView webview = (WebView) findViewById(R.id.webView);
        webview.loadUrl(itemUrl);

        final TextView urlText = (TextView) findViewById(R.id.urlText);//Test TextView --> putExtras works fine
        urlText.setText(itemUrl);





    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_web_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}